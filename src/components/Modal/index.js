import React, { useState, useEffect, useRef, useCallback } from "react";
import { BackButton } from "../../images";
import "./styles.scss";

const Modal = ({ handleClick, children }) => {
  const [isModalOpen, setModalOpen] = useState(null);
  const modalRef = useRef();
  const hideModal = useCallback(
    (e) => {
      if (isModalOpen && e.target === modalRef.current) {
        setModalOpen(false);
      }
    },
    [isModalOpen]
  );

  useEffect(() => {
    setModalOpen(children ? true : false);
    if (isModalOpen) window.addEventListener("click", hideModal);
    return () => {
      window.removeEventListener("click", hideModal);
      if (isModalOpen) handleClick();
    };
  }, [children, isModalOpen, handleClick, hideModal]);

  return isModalOpen ? (
    <div ref={modalRef} className={"modal"}>
      <div className="modal-content">
        <button className="back-btn" onClick={() => setModalOpen(false)}>
          <img src={BackButton} alt="back" />
        </button>
        {children}
      </div>
    </div>
  ) : null;
};

export default Modal;
