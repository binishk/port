import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import ProjectDetail from "./components/projectdetail";
import Home from "./views/home";
import NavBar from '../src/components/navbar';
import NotFound from '../src/components/notfound'

const BaseRouter = (props) => {
  return (
    <Router>
      <NavBar />
      <Switch>
        <Route path="/projectdetail/:projectName"  exact component={ProjectDetail} />
        <Route path="/" exact component={Home} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  );
};

export default BaseRouter;