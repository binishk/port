import React from "react";
import "./styles.scss";
import {
  BackgroundSVG,
  LinkedInLogo,
  GithubLogo,
} from "../../images";

const NewLink = ({to, children}) => {
  return (
    <a href={to} target="_blank" rel="noopener noreferrer">{children}</a>
  )
}

const Landing = () => {
  return (
    <div className="landing">
      <img className="background-bottom" src={BackgroundSVG} alt="svg" />
      <div className="contact-icons">
        <NewLink to="https://www.linkedin.com/in/binishk"><img src={LinkedInLogo} alt="linkedin" className="contact-icon" /></NewLink>
        <NewLink to="https://www.github.com/bjnish8"><img src={GithubLogo} alt="github" className="contact-icon" /></NewLink>
      </div>
      <div className="content">
        <div className="welcome">
          <h1>hi, i am binish</h1>
          <h2>full stack developer</h2>
        </div>
      </div>
    </div>
  );
};

export default Landing;
