import React from 'react';
import './styles.scss';
import Landing from '../../components/landing';
import Projects from '../../components/projects';
import Footer from '../../components/footer'

const LandingHome = () => {
    return ( 
        <div className="home">
            <Landing />
            <Projects />
            <Footer />
        </div>
     );
}
 
export default LandingHome;