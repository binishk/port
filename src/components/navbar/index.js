import React, { useState, useEffect, useRef } from "react";
import "./styles.scss";
import { Link, withRouter } from "react-router-dom";
import { MenuIcon } from "../../images";
import Modal from "../Modal"

const Contact = () => {
  const [btnText, setBtnText] = useState("copy")
  const email = "koiralabinish@gmail.com"
  const emailRef = useRef()
  const handleCopy = () => {
    setBtnText("copied!")
    setTimeout(() => {
      setBtnText("copy")
    }, 2500)
    emailRef.current.select()
    document.execCommand('copy')
  }
  return (
      <div className="contact">
       <h2>contact me at</h2>
       <ul>
         <li><svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
          <path strokeLinecap="round" strokeLinejoin="round" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z" />
        </svg><b>:</b>
        <input value={email} onChange={()=>{}} ref={emailRef}/>
        <button onClick={handleCopy}>{btnText}</button>
        </li>
         <li>
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path strokeLinecap="round" strokeLinejoin="round" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z" />
          </svg><b>:</b>
          <span>(567) 319-2308</span>
         </li>
       </ul>
      </div>
  )
}


const NavBar = (props) => {
  const [headerclass, setHeaderclass] = useState("");
  const [isSidebarVisible, setSidebarVisible] = useState(null);
  const [sidebarClass, setSideBarClass] = useState("");
  const [isContactsVisible, setContactsVisible] = useState(false)

  const sideRef = useRef();

  const updateNavHeader = () => {
    setHeaderclass(window.scrollY === 0 ? "at-top" : "");
  };

  const handleSideBar = (e) => {
    e.preventDefault();
    if (e.target === sideRef.current) {
      setSidebarVisible(false);
    }
  };

  useEffect(() => {
    if (isSidebarVisible) setSidebarVisible(false);
    if (props.location.hash === "#portfolio") {
      try {
        window.scroll({
          top: 0.9 * window.innerHeight,
          left: 0,
          behavior: "smooth",
        });
      } catch (error) {
        window.scrollTo(0, 0);
      }
    } else if (props.location.hash === "#home") {
      try {
        window.scroll({
          top: 0,
          left: 0,
          behavior: "smooth",
        });
      } catch (error) {
        window.scrollTo(0, 0);
      }
    }
    window.addEventListener("scroll", updateNavHeader);
    return () => {
      window.removeEventListener("scroll", updateNavHeader);
    };
  }, [props.location.hash]);

  useEffect(() => {
    if (isSidebarVisible !== null) {
      setSideBarClass(isSidebarVisible ? "slidein" : "slideout");
      if (isSidebarVisible) {
        window.addEventListener("click", handleSideBar);
      }
    }

    return () => {
      window.removeEventListener("click", handleSideBar);
    };
  }, [isSidebarVisible]);

  return (
    <React.Fragment>
      <nav className={`navbar ${headerclass}`}>
        <div className="links">
          <img
            className="menu"
            src={MenuIcon}
            alt="menu"
            onClick={() => setSidebarVisible(!isSidebarVisible)}
          />
          <Link className="nav-link" to="/#home">
            Home
          </Link>
          <button className="nav-link" onClick={() => {setContactsVisible(true)}}>
            Contact
          </button>
          <Link className="nav-link" to="/#portfolio">
            Portfolio
          </Link>
        </div>
      </nav>
      <div className="navbar-filler" />
      <div ref={sideRef} className={`sidebar  ${sidebarClass}`}>
        <nav className={`sidebar-content`}>
          <ul className="sidebar-links">
            <li>
              <Link className="side-link" to="/#home">
                Home
              </Link>
            </li>
            <li>
              <Link className="side-link" to="/#portfolio">
                Portfolio
              </Link>
            </li>
            <li>
              <button className="side-link" onClick={() => {setContactsVisible(true)}}>
                Contact
              </button>
            </li>
          <span>binishkoirala@gmail.com</span>
          </ul>
        </nav>
      </div>
      {isContactsVisible && <Modal handleClick={() => {setContactsVisible(false)}}><Contact /></Modal>}
    </React.Fragment>
  );
};

export default withRouter(NavBar);
