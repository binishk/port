import React from 'react';
import './App.scss';
import BaseRouter from "./routes";

function App(props) {
  return (
    <div className="app">
      <BaseRouter {...props}/>
    </div>
  );
}

export default App;
