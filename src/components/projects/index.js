import React, { useState, useCallback } from "react";
import "./styles.scss";
import {
  AugventsPNG,
  ShellshotsPNG,
  TwitchStitchPNG,
  TagWithMePNG,
  RepoPNG
} from "../../images";
import Modal from "../Modal";
import ProjectDetail from "../projectdetail";

const Project = ({ projectName, setChosenProject, children, history }) => {
  return (
    <div
      className="project-app"
      onClick={() => {
        setChosenProject(projectName);
      }}
    >
      {children}
      <button className="learn">Learn More</button>
    </div>
  );
};

const allProjects = [
  {
    name: "shellshots",
    imgsrc: ShellshotsPNG,
  },
  {
    name: "augvents",
    imgsrc: AugventsPNG,
  },
  {
    name: "twitchstitch",
    imgsrc: TwitchStitchPNG,
  },
  {
    name: "tagwithme",
    imgsrc: TagWithMePNG,
  },
  {
    name: "repo",
    imgsrc: RepoPNG
  }
];

const Projects = () => {
  const [chosenProject, setChosenProject] = useState(null);

  const hideProjectDetails = useCallback(() => {
    setChosenProject(null);
  }, []);

  return (
    <div className="portfolio">
      <h1>Portfolio</h1>
      {chosenProject && (
        <Modal handleClick={hideProjectDetails}>
          <ProjectDetail project={chosenProject} />
        </Modal>
      )}
      <div className="content">
        {allProjects.map(({ name, imgsrc }) => {
          return (
            <Project
              projectName={name}
              key={name}
              setChosenProject={setChosenProject}
            >
             <div className="image-container"> <img src={imgsrc} alt={name} /></div>
            </Project>
          );
        })}
      </div>
    </div>
  );
};

export default Projects;
